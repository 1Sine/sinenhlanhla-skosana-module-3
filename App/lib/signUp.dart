import 'package:flutter/material.dart';
import 'package:flutter_application_1/main.dart';

class signupscreen extends StatelessWidget {
  const signupscreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SHOPPING SPREE',
      home: Scaffold(
        appBar: AppBar(title: const Text('Sign up')),
        body: const signupScreen(),
      ),
    );
  }
}

class signupScreen extends StatefulWidget {
  const signupScreen({Key? key}) : super(key: key);

  @override
  _signupScreenState createState() => _signupScreenState();
}

class _signupScreenState extends State<signupScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.person),
                hintText: 'Enter name',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.person),
                hintText: 'Enter surname',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.phone),
                hintText: 'Enter pnone number',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              obscureText: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.lock),
                hintText: 'Enter password',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.email),
                hintText: 'Enter email address',
              ),
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              child: const Text('Sign up'),
              onPressed: () {},
            ),
          ),
          SizedBox(
              width: double.infinity,
              child: TextButton(
                child: const Text('Return to Login'),
                onPressed: () {
                  ('Button is clicked');
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const loginScreen()),
                  );
                },
              )),
        ]));
  }
}
