import 'package:flutter/material.dart';
import 'package:flutter_application_1/main.dart';
import 'package:flutter_application_1/signup.dart';
import 'package:flutter_application_1/user_profile.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SHOPPING SPREE',
      home: Scaffold(
        appBar: AppBar(title: const Text('Dashboard')),
        body: const dashBoard(),
      ),
    );
  }
}

class dashBoard extends StatefulWidget {
  const dashBoard({Key? key}) : super(key: key);

  @override
  _dashBoardState createState() => _dashBoardState();
}

class _dashBoardState extends State<dashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            const Image(
              image: AssetImage('images/summer.png'),
              width: 80,
              height: 80,
            ),
            TextButton(
              child: const Text('Summer collection'),
              onPressed: () {},
            ),
            const Image(
              image: AssetImage('images/winter.jpg'),
              width: 80,
              height: 80,
            ),
            TextButton(
              child: const Text('Winter collection'),
              onPressed: () {},
            ),
            Row(
              mainAxisAlignment: (MainAxisAlignment.center),
              children: <Widget>[
                TextButton(
                  child: const Text('Return to Login'),
                  onPressed: () {
                    ('Button is clicked');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const loginScreen()),
                    );
                  },
                ),
                TextButton(
                  child: const Text('Go to sign up'),
                  onPressed: () {
                    ('Button is clicked');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const signupscreen()),
                    );
                  },
                ),
                TextButton(
                  child: const Text('Go to user profile'),
                  onPressed: () {
                    ('Button is clicked');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const userProfile()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
