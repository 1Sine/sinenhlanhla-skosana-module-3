import 'package:flutter/material.dart';
import 'package:flutter_application_1/dashboard.dart';
import 'package:flutter_application_1/signup.dart';

void main() {
  runApp(loginScreen());
}

class loginScreen extends StatelessWidget {
  const loginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SHOPPING SPREE',
      home: Scaffold(
        appBar: AppBar(title: const Text('Login')),
        body: const LoginScreen(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const dashboard()));
          },
          child: const Icon(Icons.dashboard),
        ),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(10),
          child: const TextField(
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              prefixIcon: Icon(Icons.person),
              hintText: 'User name',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              prefixIcon: Icon(Icons.lock),
              hintText: 'Password',
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: ElevatedButton(
            child: const Text('LOGIN'),
            onPressed: () {
              ('Button is clicked');
            },
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: TextButton(
            child: const Text('Forgot password'),
            onPressed: () {
              ('Button is clicked');
            },
          ),
        ),
        Row(
          mainAxisAlignment: (MainAxisAlignment.center),
          children: <Widget>[
            const Text('Does not have an account'),
            TextButton(
              child: const Text('Sign up'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const signupscreen()),
                );
              },
            ),
          ],
        ),
      ],
    ));
  }
}
