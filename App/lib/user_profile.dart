import 'package:flutter/material.dart';
import 'package:flutter_application_1/main.dart';

class userProfile extends StatelessWidget {
  const userProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SHOPPING SPREE',
      home: Scaffold(
        appBar: AppBar(title: const Text('User profile')),
        body: const userprofile(),
      ),
    );
  }
}

class userprofile extends StatefulWidget {
  const userprofile({Key? key}) : super(key: key);

  @override
  _userprofileState createState() => _userprofileState();
}

class _userprofileState extends State<userprofile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.person),
                hintText: 'Enter name',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.person),
                hintText: 'Enter surname',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.house),
                hintText: 'Enter address',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.phone),
                hintText: 'Enter phone number',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            child: const TextField(
              obscureText: true,
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                prefixIcon: Icon(Icons.lock),
                hintText: 'Enter password',
              ),
            ),
          ),
          SizedBox(
              width: double.infinity,
              child: TextButton(
                child: const Text('Save changes'),
                onPressed: () {
                  ('Button is clicked');
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const loginScreen()),
                  );
                },
              )),
        ]));
  }
}
